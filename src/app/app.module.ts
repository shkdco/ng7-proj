import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';


import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import {environment} from '../environments/environment';

import {Routes, RouterModule} from '@angular/router';

import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatTableModule} from '@angular/material/table';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import {MatCheckboxModule} from '@angular/material/checkbox';



@NgModule({
  declarations: [
    AppComponent,
    NavComponent
  ],
  imports: [
    BrowserModule
//  FormsModule,
   // MatCardModule, 
   // MatButtonModule,
   // MatFormFieldModule,
   // MatInputModule,
 //   MatListModule,
  //  MatCheckboxModule,
 //   BrowserAnimationsModule,
  //  AngularFireAuthModule,
 //   AngularFireDatabaseModule,
   // AngularFireModule.initializeApp(environment.firebase),        
//    RouterModule.forRoot([
  //    {path:'',component:ItemsComponent},
  //    {path:'login',component:LoginComponent},
  //    {path:'**',component:NotfoundComponent},      
   // ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
